#!/bin/bash

SEPARATOR=';'
COLUMN="3"

while read -r INPUT; do
	echo "$INPUT" | cut -d"$SEPARATOR" -f"$COLUMN"
done
